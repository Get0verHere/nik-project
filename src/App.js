import React from 'react';
import './App.css';
import ProfileContainer from "./components/Profile/ProfileContainer";
import Header from "./components/Header/Header";
import Route from "react-router-dom/es/Route";

function App() {
    return (<div>
        <Header/>
        <Route path="/profile/:userId?" render={() => <ProfileContainer />}/>
    </div>)
}

export default App;
