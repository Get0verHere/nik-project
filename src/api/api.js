import * as axios from "axios";

const instance = axios.create({
    baseURL: 'https://2ku0y0.deta.dev/'
});

const getModuleStats = (login, skipUnreviewed) => {
    return instance.get(`profile/${login}?skip_unreviewed=${skipUnreviewed}`)
        .then(response => {
            return response.data.modules_stats;
        })
}

export const api = {
    getModuleStats: getModuleStats
}
