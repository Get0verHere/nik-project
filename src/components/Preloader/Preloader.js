import spinnerImg from "../../assets/images/spinner.svg";
import React from "react";

const Preloader = (props) => {
    return <div style={{backgroundColor: 'white'}}>
        <img src={spinnerImg}/>
    </div>
};

export default Preloader;