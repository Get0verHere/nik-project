import React from "react";
import Preloader from "../Preloader/Preloader";

const Profile = (props) => {
    const modules_stats = props.profilePage.profile.modules_stats
    return <div>
        <label htmlFor="skipUnreviewed"><input type="checkbox" checked={props.profilePage.skipUnreviewed}
                                               onChange={props.onSkipUnreviewedChanged}
                                               disabled={props.profilePage.isFetching}
        />Skip unreviewed
        </label>
        {props.profilePage.isFetching ? <Preloader/> : <table border="1">
            <thead>
            <tr>
                <td>module_name</td>
                <td>mean</td>
                <td>median</td>
            </tr>
            </thead>
            <tbody>
            {
                modules_stats && modules_stats.map(m => <tr>
                    <td>{m.module_name}</td>
                    <td>{m.mean}</td>
                    <td>{m.median}</td>
                </tr>)
            }
            </tbody>
        </table>}
    </div>
}

export default Profile;