import React from 'react';
import Profile from './Profile';
import {getModuleStats, toggleSkipUnreviewed} from "../../redux/profile-reducer";
import {connect} from "react-redux";
import {withRouter} from "react-router-dom"

class ProfileContainer extends React.Component {
    getUserId() {
        const userId = this.props.match.params.userId;
        if (!userId) {
            return 'nik726';
        }
        return userId;
    }

    componentDidMount() {
        this.props.getModuleStats(this.getUserId(), this.props.profilePage.skipUnreviewed);
    }

    onSkipUnreviewedChanged = (e) => {
        const checked = e.target.checked
        this.props.toggleSkipUnreviewed(checked)
        this.props.getModuleStats(this.getUserId(), checked);
    }

    render() {
        return <Profile profilePage={this.props.profilePage}
                        onSkipUnreviewedChanged={this.onSkipUnreviewedChanged}/>
    }
}

const mapStateToProps = (state) => ({
    profilePage: state.profilePage
});

let WithUrlDataContainerComponent = withRouter(ProfileContainer);

export default connect(mapStateToProps, {getModuleStats, toggleSkipUnreviewed})(WithUrlDataContainerComponent);