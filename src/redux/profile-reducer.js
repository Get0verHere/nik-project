import {api} from "../api/api";
const SET_MODULES_STATS = 'SET_MODULES_STATS';
const TOGGLE_SKIP_UNREVIEWED = 'TOGGLE_SKIP_UNREVIEWED';
const TOGGLE_IS_FETCHING = 'TOGGLE_IS_FETCHING';

const initialState = {
    skipUnreviewed: false,
    profile: {
        modules_stats: []
    },
    isFetching: false
}

const profileReducer = (state = initialState, action) => {
    switch (action.type) {
        case SET_MODULES_STATS:
            return {
                ...state,
                profile: {
                    ...state.profile,
                    modules_stats: action.moduleStats
                }
            };
        case TOGGLE_SKIP_UNREVIEWED:
            return {
                ...state,
                skipUnreviewed: action.skipUnreviewed
            }
        case TOGGLE_IS_FETCHING:
            return {
                ...state,
                isFetching: action.isFetching
            }
        default:
            return state;
    }
};

const setModuleStats = (moduleStats) => ({
    type: SET_MODULES_STATS,
    moduleStats
});

const toggleIsFetching = (isFetching) => ({
    type: TOGGLE_IS_FETCHING,
    isFetching
})

export const getModuleStats = (userId, skipUnreviewed) => {
    return (dispatch) => {
        dispatch(toggleIsFetching(true))
        api.getModuleStats(userId, skipUnreviewed)
            .then(response => {
                dispatch(setModuleStats(response));
            })
            .finally(() => {dispatch(toggleIsFetching(false))})
    }
}

export const toggleSkipUnreviewed = (skipUnreviewed) => ({
    type: TOGGLE_SKIP_UNREVIEWED,
    skipUnreviewed
})

export default profileReducer;